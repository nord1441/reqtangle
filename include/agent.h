#pragma once
#include "common.h"

void initNetwork(Agent *);
void calculateQ(Agent *);
int decideAction(Agent *);
double updateNet(Agent *, Agent *);
History * allocHistory(unsigned int, unsigned int);
void freeHistory(History *);
void destroyHistoryList(List *);
int importAgentValues(Agent *);
int prepareExportAgentValue(Agent *);
int exportAgentValue(Agent *);
void initAgent(Agent *);
void freeAgent(Agent *);
void addHistory(Agent *);
void forwardAgentStep(Agent *);
