#pragma once
#include "common.h"

typedef struct _sMaxNormRandPolicy {
	Policy base;
	double sigma;
} SelectMaxNormRandPolicy;

typedef struct _eGreedyPolicy {
	Policy base;
	double e;
} EpsilonGreedyPolicy;

typedef struct _aEpsilonGreedyPolicy {
	Policy base;
	double atte;
	double e;
} AdaptiveEpsilonGreedyPolicy;

typedef struct _ucb1Policy {
	Policy base;
	unsigned int * rewardCount;
	unsigned int * countSelected;
	double c;
	int * totalReward;
	char * outFileName;
} Ucb1Policy;

typedef struct _bSamplingPolicy {
	Policy base;
	unsigned int * countSelected;
	int * totalReward;
	char * outFileName;
} BayesSamplingPolicy;

typedef struct _preservedPolicy {
	Policy base;
	double * policyValue;
	char * inFileName;
} PreservedPolicy;

double normRand(double, double);
Policy * initPolicySelectMax(void);
Policy * initPolicySelectMaxNormRand(double);
Policy * initPolicyEpsilonGreedy(double);
Policy * initPolicyAdaptiveEpsilonGreedy(double, double);
Policy * initPolicyUcb1(unsigned int, double);
Policy * initPolicyBayesSampling(unsigned int);
Policy * initPolicyPreserved(unsigned int);
void freePolicy(Policy *);
void freePolicySelectMaxNormRand(Policy *);
void freePolicyEpsilonGreedy(Policy *);
void freePolicyAdaptiveEpsilonGreedy(Policy *);
void freePolicyUcb1(Policy *);
void freePolicyBayesSampling(Policy *);
void freePolicyPreserved(Policy *);
double normRand(double, double);
int selectMax(Agent *, Policy *);
int selectMaxNormRand(Agent *, Policy *);
int epsilonGreedy(Agent *, Policy *);
int adaptiveEpsilonGreedy(Agent *, Policy *);
int ucb1(Agent *, Policy *);
int bayesSampling(Agent *, Policy *);
int preserved(Agent *, Policy *);
Policy * parsePolicyOptionSelectMaxNormRand(char *, Policy *);
Policy * parsePolicyOptionEpsilonGreedy(char *, Policy *);
Policy * parsePolicyOptionAdaptiveEpsilonGreedy(char *, Policy *);
Policy * parsePolicyOptionUcb1(char *, Policy *);
Policy * parsePolicyOptionBayesSampling(char *, Policy *);
Policy * parsePolicyOptionPreserved(char *, Policy *);



