#pragma once
#include "common.h"

typedef struct _zeroStepSarsaTd {
	Td base;
	double gamma;
	List * histories;
	History * targetUpdate;
} ZeroStepSarsaTd;

typedef struct _zeroStepQTd {
	Td base;
	double gamma;
	List * histories;
	History * targetUpdate;
} ZeroStepQTd;

typedef struct _nStepSarsaTd {
	Td base;
	double gamma;
	unsigned int intervalUpdate;
	int count;
	int totalCount;
	List * histories;
	History * targetUpdate;
} NStepSarsaTd;

typedef struct _nStepQTd {
	Td base;
	double gamma;
	unsigned int intervalUpdate;
	int count;
	int totalCount;
	List * histories;
	History * targetUpdate;
} NStepQTd;

Td * initTdZeroStepSarsa(double);
Td * initTdZeroStepQ(double);
Td * initTdNStepSarsa(double, unsigned int);
Td * initTdNStepQ(double, unsigned int);
void freeTdZeroStepSarsa(Td *);
void freeTdZeroStepQ(Td *);
void freeTdNStepSarsa(Td *);
void freeTdNStepQ(Td *);
double * getZeroStepSarsaTd(Agent *);
double * getZeroStepQTd(Agent *);
double * getNStepSarsaTd(Agent *);
double * getNStepQTd(Agent *);
Td * parseTdOptionZeroStepSarsa(char *, Td *);
Td * parseTdOptionZeroStepQ(char *, Td *);
Td * parseTdOptionNStepSarsa(char *, Td *);
Td * parseTdOptionNStepQ(char *, Td *);
