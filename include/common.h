#pragma once
#include <stdbool.h>
#include <string.h>
#include <math.h>
#include "nlist.h"
#include "nmath.h"
#include "nnet.h"

struct _agent;

typedef struct _td {
	double *(* getTd)(struct _agent *);
	void (* free)(struct _td *);
} Td;

typedef struct _policy {
	int (* applyPolicy)(struct _agent *, struct _policy *);
	void (* freePolicy)(struct _policy *);
} Policy;

typedef struct _history {
	double * Q;
	int direction;
	double * sensor;
	int reward;
} History;

typedef enum {
	learn, eval
} Mode;

typedef struct _agent {
	FILE * fpQUpdate;
	unsigned int countLayer;
	unsigned int * countNeuron;
	double netAlpha;
	double netRandMax;
	List * network;
	List * targetNetwork;
	Td * td;
	Policy * policy;
	char * fileInput;
	char * fileOutput;
	double polTemp[3];
	List * histories;
	unsigned int countSensor;
	double * QTargetNetwork;
	unsigned int iterTargetNetwork;
	unsigned int periodUpdateTargetNetwork;
	List * accumulatedWeights;
	bool isAsync;
} Agent;

