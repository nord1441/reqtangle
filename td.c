#include "include/agent.h"
#include "include/td.h"
#include <stdlib.h>
#include <stdbool.h>

Td * initTdZeroStepSarsa(double gamma) {
	ZeroStepSarsaTd * zeroStepSarsaTd = calloc(1, sizeof(ZeroStepSarsaTd));
	if(!zeroStepSarsaTd) return NULL;
	zeroStepSarsaTd->base.getTd = getZeroStepSarsaTd;
	zeroStepSarsaTd->base.free = freeTdZeroStepSarsa;
	zeroStepSarsaTd->gamma = gamma;
	zeroStepSarsaTd->histories = calloc(1, sizeof(List));
	if(!zeroStepSarsaTd->histories) return NULL;
	return &(zeroStepSarsaTd->base);
}

Td * initTdZeroStepQ(double gamma) {
	ZeroStepQTd * zeroStepQTd = calloc(1, sizeof(ZeroStepQTd));
	if(!zeroStepQTd) return NULL;
	zeroStepQTd->base.getTd = getZeroStepQTd;
	zeroStepQTd->base.free = freeTdZeroStepQ;
	zeroStepQTd->gamma = gamma;
	zeroStepQTd->histories = calloc(1, sizeof(List));
	if(!zeroStepQTd->histories) return NULL;
	return &(zeroStepQTd->base);
}

Td * initTdNStepSarsa(double gamma, unsigned int intervalUpdate) {
	NStepSarsaTd * nStepSarsaTd = calloc(1, sizeof(NStepSarsaTd));
	if(!nStepSarsaTd) return NULL;
	nStepSarsaTd->base.getTd = getNStepSarsaTd;
	nStepSarsaTd->base.free = freeTdNStepSarsa;
	nStepSarsaTd->gamma = gamma;
	if(intervalUpdate == 0) nStepSarsaTd->intervalUpdate = 1;
	else nStepSarsaTd->intervalUpdate = intervalUpdate;
	nStepSarsaTd->count = nStepSarsaTd->intervalUpdate;
	nStepSarsaTd->histories = calloc(1, sizeof(List));
	if(!nStepSarsaTd->histories) return NULL;
	return &(nStepSarsaTd->base);
}

Td * initTdNStepQ(double gamma, unsigned int intervalUpdate) {
	NStepQTd * nStepQTd = calloc(1, sizeof(NStepQTd));
	if(!nStepQTd) return NULL;
	nStepQTd->base.getTd = getNStepQTd;
	nStepQTd->base.free = freeTdNStepQ;
	nStepQTd->gamma = gamma;
	if(intervalUpdate == 0) nStepQTd->intervalUpdate = 1;
	else nStepQTd->intervalUpdate = intervalUpdate;
	nStepQTd->count = nStepQTd->intervalUpdate;
	nStepQTd->histories = calloc(1, sizeof(List));
	if(!nStepQTd->histories) return NULL;
	return &(nStepQTd->base);
}

void freeTdZeroStepSarsa(Td * td) {
	ZeroStepSarsaTd * zeroStepSarsaTd = (ZeroStepSarsaTd *)td;
	while(zeroStepSarsaTd->histories->head) delTail(zeroStepSarsaTd->histories);
	free(zeroStepSarsaTd->histories);
	free((ZeroStepSarsaTd *)td);
}

void freeTdZeroStepQ(Td * td) {
	ZeroStepQTd * zeroStepQTd = (ZeroStepQTd *)td;
	while(zeroStepQTd->histories->head) delTail(zeroStepQTd->histories);
	free(zeroStepQTd->histories);
	free((ZeroStepQTd *)td);
}

void freeTdNStepSarsa(Td * td) {
	NStepSarsaTd * nStepSarsaTd = (NStepSarsaTd *)td;
	while(nStepSarsaTd->histories->head) delTail(nStepSarsaTd->histories);
	free(nStepSarsaTd->histories);
	free((NStepSarsaTd *)td);
}

void freeTdNStepQ(Td * td) {
	NStepQTd * nStepQTd = (NStepQTd *)td;
	while(nStepQTd->histories->head) delTail(nStepQTd->histories);
	free(nStepQTd->histories);
	free((NStepQTd *)td);
}

double * getZeroStepSarsaTd(Agent * agent) {
	ZeroStepSarsaTd * zeroStepSarsaTd = ((ZeroStepSarsaTd *)(agent->td));
	addTail(zeroStepSarsaTd->histories);
	zeroStepSarsaTd->histories->tail->data = agent->histories->tail->data;
	double * error = calloc(agent->countNeuron[agent->countLayer - 1], sizeof(double));
	if(!error) return NULL;
	if(zeroStepSarsaTd->histories->head == zeroStepSarsaTd->histories->tail) {
		zeroStepSarsaTd->targetUpdate = zeroStepSarsaTd->histories->tail->data;
		for(unsigned int i = 0; i < agent->countNeuron[agent->countLayer - 1]; i++) error[i] = 0;
	} else {
		zeroStepSarsaTd->targetUpdate = zeroStepSarsaTd->histories->head->data;
		for(unsigned int i = 0; i < agent->countNeuron[agent->countLayer - 1]; i++) {
			History * currentHistory = ((History *)(zeroStepSarsaTd->histories->tail->data));
			if(i==zeroStepSarsaTd->targetUpdate->direction) {
				if(currentHistory->reward == 0) error[i] = currentHistory->reward + zeroStepSarsaTd->gamma * agent->QTargetNetwork[currentHistory->direction] - zeroStepSarsaTd->targetUpdate->Q[zeroStepSarsaTd->targetUpdate->direction];
				else error[i] = currentHistory->reward - zeroStepSarsaTd->targetUpdate->Q[zeroStepSarsaTd->targetUpdate->direction];
			}
			else error[i] = 0;
		}
		while(zeroStepSarsaTd->histories->head) delTail(zeroStepSarsaTd->histories);
		addTail(zeroStepSarsaTd->histories);
		zeroStepSarsaTd->histories->tail->data = agent->histories->tail->data;
	}
	return error;
}

double * getZeroStepQTd(Agent * agent) {
	ZeroStepQTd * zeroStepQTd = ((ZeroStepQTd *)(agent->td));
	addTail(zeroStepQTd->histories);
	zeroStepQTd->histories->tail->data = agent->histories->tail->data;
	double * error = calloc(agent->countNeuron[agent->countLayer - 1], sizeof(double));
	if(!error) return NULL;
	double max = -1 * __DBL_MAX__;
	bool isMax[] = {false, false, false};
        unsigned int count = 0;
        for(unsigned int i = 0; i < agent->countNeuron[agent->countLayer - 1]; i++) if(((History *)(zeroStepQTd->histories->tail->data))->Q[i] > max) max = ((History *)(zeroStepQTd->histories->tail->data))->Q[i];
        for(unsigned int i = 0; i < agent->countNeuron[agent->countLayer - 1]; i++) {
                if(((History *)(zeroStepQTd->histories->tail->data))->Q[i] == max) {
                        isMax[i] = true;
                        count++;
                }
        }
        int d = 0;
        int rnd = (int)(drand48() * count);
        for(unsigned int i = 0; i < agent->countNeuron[agent->countLayer - 1]; i++) {
                if(isMax[i] == true) {
                        if(rnd > 0) {
                                rnd --;
                        } else {
                                d = i;
                                break;
                        }
                        d = i;
                }
        }
	if(zeroStepQTd->histories->head == zeroStepQTd->histories->tail) {
		zeroStepQTd->targetUpdate = zeroStepQTd->histories->tail->data;
		for(unsigned int i = 0; i < agent->countNeuron[agent->countLayer - 1]; i++) error[i] = 0;
	} else {
		zeroStepQTd->targetUpdate = zeroStepQTd->histories->head->data;
		for(unsigned int i = 0; i < agent->countNeuron[agent->countLayer - 1]; i++) {
			History * currentHistory = ((History *)(zeroStepQTd->histories->tail->data));
			if(i==zeroStepQTd->targetUpdate->direction) {
				if(currentHistory->reward == 0) error[i] = currentHistory->reward + zeroStepQTd->gamma * agent->QTargetNetwork[d] - zeroStepQTd->targetUpdate->Q[zeroStepQTd->targetUpdate->direction];
				else error[i] = currentHistory->reward - zeroStepQTd->targetUpdate->Q[zeroStepQTd->targetUpdate->direction];
			}
			else error[i] = 0;
		}
		while(zeroStepQTd->histories->head) delTail(zeroStepQTd->histories);
		addTail(zeroStepQTd->histories);
		zeroStepQTd->histories->tail->data = agent->histories->tail->data;
	}
	return error;
}

double * getNStepSarsaTd(Agent * agent) {
	double * error = calloc(agent->countNeuron[agent->countLayer - 1], sizeof(double));
	if(!error) return NULL;
	NStepSarsaTd * nStepSarsaTd = ((NStepSarsaTd *)(agent->td));
	addTail(nStepSarsaTd->histories);
	nStepSarsaTd->histories->tail->data = agent->histories->tail->data;
	if(nStepSarsaTd->totalCount == 0 || nStepSarsaTd->count == 0 || ((History *)(nStepSarsaTd->histories->tail->data))->reward != 0) nStepSarsaTd->targetUpdate = nStepSarsaTd->histories->tail->data;
	if(nStepSarsaTd->count == 0 || ((History *)(nStepSarsaTd->histories->tail->data))->reward != 0) {
		for(unsigned int i = 0; i < agent->countNeuron[agent->countLayer - 1]; i++) {
			if(i == ((History *)(nStepSarsaTd->histories->head->data))->direction) {
				double gamma = 1;
				Node * current = nStepSarsaTd->histories->head;
				error[i] -= ((History *)(current->data))->Q[i];
				current = current->next;
				while(current) {
					error[i] += gamma * ((History *)(current->data))->reward;
					gamma *= nStepSarsaTd->gamma;
					current = current->next;
				}
				if(nStepSarsaTd->count < nStepSarsaTd->intervalUpdate - 1) error[i] += nStepSarsaTd->gamma * agent->QTargetNetwork[((History *)(nStepSarsaTd->histories->tail->data))->direction];
			} else {
				error[i] = 0;
			}
		}
		while(nStepSarsaTd->histories->head) delTail(nStepSarsaTd->histories);
		addTail(nStepSarsaTd->histories);
		nStepSarsaTd->histories->tail->data = agent->histories->tail->data;
		nStepSarsaTd->count = nStepSarsaTd->intervalUpdate;
	} else {
		for(unsigned int i = 0; i < agent->countNeuron[agent->countLayer - 1]; i++) {
			error[i] = 0;
		}
	}
	nStepSarsaTd->count--;
	nStepSarsaTd->totalCount++;
	return error;
}

double * getNStepQTd(Agent * agent) {
	double * error = calloc(agent->countNeuron[agent->countLayer - 1], sizeof(double));
	if(!error) return NULL;
	NStepQTd * nStepQTd = ((NStepQTd *)(agent->td));
	addTail(nStepQTd->histories);
	nStepQTd->histories->tail->data = agent->histories->tail->data;
	if(nStepQTd->totalCount == 0 || nStepQTd->count == 0 || ((History *)(nStepQTd->histories->tail->data))->reward != 0) nStepQTd->targetUpdate = nStepQTd->histories->tail->data;
	if(nStepQTd->count == 0 || ((History *)(nStepQTd->histories->tail->data))->reward != 0) {
		for(unsigned int i = 0; i < agent->countNeuron[agent->countLayer - 1]; i++) {
			if(i == ((History *)(nStepQTd->histories->head->data))->direction) {
				double gamma = 1;
				Node * current = nStepQTd->histories->head;
				error[i] -= ((History *)(current->data))->Q[i];
				current = current->next;
				while(current) {
					error[i] += gamma * ((History *)(current->data))->reward;
					gamma *= nStepQTd->gamma;
					current = current->next;
				}
				double max = -1 * __DBL_MAX__;
				bool isMax[] = {false, false, false};
				unsigned int count = 0;
				for(unsigned int i = 0; i < agent->countNeuron[agent->countLayer - 1]; i++) if(((History *)(nStepQTd->histories->tail->data))->Q[i] > max) max = ((History *)(nStepQTd->histories->tail->data))->Q[i];
				for(unsigned int i = 0; i < agent->countNeuron[agent->countLayer - 1]; i++) {
					if(((History *)(nStepQTd->histories->tail->data))->Q[i] == max) {
						isMax[i] = true;
						count++;
					}
				}
				int d = 0;
				int rnd = (int)(drand48() * count);
				for(unsigned int i = 0; i < agent->countNeuron[agent->countLayer - 1]; i++) {
					if(isMax[i] == true) {
						if(rnd > 0) {
							rnd --;
						} else {
							d = i;
							break;
						}
						d = i;
					}
				}
				if(nStepQTd->count < nStepQTd->intervalUpdate - 1) error[i] += nStepQTd->gamma * agent->QTargetNetwork[d];
			} else {
				error[i] = 0;
			}
		}
		while(nStepQTd->histories->head) delTail(nStepQTd->histories);
		addTail(nStepQTd->histories);
		nStepQTd->histories->tail->data = agent->histories->tail->data;
		nStepQTd->count = nStepQTd->intervalUpdate;
	} else {
		for(unsigned int i = 0; i < agent->countNeuron[agent->countLayer - 1]; i++) {
			error[i] = 0;
		}
	}
	nStepQTd->count--;
	nStepQTd->totalCount++;
	return error;
}

Td * parseTdOptionZeroStepSarsa(char * tdParams, Td * td){
	char * query, * pTdParams;
	query = "gamma=";
	pTdParams = strstr(tdParams, query);
	if(!pTdParams) return NULL;
	else pTdParams = strchr(pTdParams, '=');
	double gamma = atof(pTdParams + 1);
	Td * ret = initTdZeroStepSarsa(gamma);
	return ret;
}

Td * parseTdOptionZeroStepQ(char * tdParams, Td * td){
	char * query, * pTdParams;
	query = "gamma=";
	pTdParams = strstr(tdParams, query);
	if(!pTdParams) return NULL;
	else pTdParams = strchr(pTdParams, '=');
	double gamma = atof(pTdParams + 1);
	Td * ret = initTdZeroStepQ(gamma);
	return ret;
}

Td * parseTdOptionNStepSarsa(char * tdParams, Td * td){
	char * query, * pTdParams;
	query = "gamma=";
	pTdParams = strstr(tdParams, query);
	if(!pTdParams) return NULL;
	else pTdParams = strchr(pTdParams, '=');
	double gamma = atof(pTdParams + 1);
	query = "step=";
	pTdParams = strstr(tdParams, query);
	if(!pTdParams) return NULL;
	else pTdParams = strchr(pTdParams, '=');
	unsigned int intervalUpdate = atoi(pTdParams + 1) + 1;
	Td * ret = initTdNStepSarsa(gamma, intervalUpdate);
	return ret;
}

Td * parseTdOptionNStepQ(char * tdParams, Td * td){
	char * query, * pTdParams;
	query = "gamma=";
	pTdParams = strstr(tdParams, query);
	if(!pTdParams) return NULL;
	else pTdParams = strchr(pTdParams, '=');
	double gamma = atof(pTdParams + 1);
	query = "step=";
	pTdParams = strstr(tdParams, query);
	if(!pTdParams) return NULL;
	else pTdParams = strchr(pTdParams, '=');
	unsigned int intervalUpdate = atoi(pTdParams + 1) + 1;
	Td * ret = initTdNStepQ(gamma, intervalUpdate);
	return ret;
}

