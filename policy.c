#include "include/agent.h"
#include "include/policy.h"
#include <stdlib.h>
#include <stdbool.h>

double normRand(double mu, double sigma){
    double z = sqrt(-2.0 * log(drand48())) * sin(2.0 * M_PI * drand48());
    return mu + sigma * z;
}

Policy * initPolicySelectMax(void) {
	Policy * policy = calloc(1, sizeof(Policy));
	if(!policy) return NULL;
	policy->applyPolicy = selectMax;
	policy->freePolicy = freePolicy;
	return policy;
}

Policy * initPolicySelectMaxNormRand(double sigma) {
	SelectMaxNormRandPolicy * sMaxNormRandPolicy = calloc(1, sizeof(SelectMaxNormRandPolicy));
	if(!sMaxNormRandPolicy) return NULL;
	sMaxNormRandPolicy->base.applyPolicy = selectMaxNormRand;
	sMaxNormRandPolicy->base.freePolicy = freePolicySelectMaxNormRand;
	sMaxNormRandPolicy->sigma = sigma;
	return &(sMaxNormRandPolicy->base);
}

Policy * initPolicyEpsilonGreedy(double e) {
	EpsilonGreedyPolicy * eGreedyPolicy = calloc(1, sizeof(EpsilonGreedyPolicy));
	if(!eGreedyPolicy) return NULL;
	eGreedyPolicy->base.applyPolicy = epsilonGreedy;
	eGreedyPolicy->base.freePolicy = freePolicyEpsilonGreedy;
	eGreedyPolicy->e = e;
	return &(eGreedyPolicy->base);
}

Policy * initPolicyAdaptiveEpsilonGreedy(double atte, double e) {
	AdaptiveEpsilonGreedyPolicy * aEpsilonGreedyPolicy = calloc(1, sizeof(AdaptiveEpsilonGreedyPolicy));
	if(!aEpsilonGreedyPolicy) return NULL;
	aEpsilonGreedyPolicy->base.applyPolicy = adaptiveEpsilonGreedy;
	aEpsilonGreedyPolicy->base.freePolicy = freePolicyAdaptiveEpsilonGreedy;
	aEpsilonGreedyPolicy->atte = atte;
	aEpsilonGreedyPolicy->e = e;
	return &(aEpsilonGreedyPolicy->base);
}

Policy * initPolicyUcb1(unsigned int count, double c) {
	Ucb1Policy * ucb1Policy = calloc(1, sizeof(Ucb1Policy));
	if(!ucb1Policy) return NULL;
	ucb1Policy->c = c;
	ucb1Policy->countSelected = calloc(count, sizeof(unsigned int));
	if(!ucb1Policy->countSelected) return NULL;
	ucb1Policy->totalReward = calloc(count, sizeof(int));
	if(!ucb1Policy->totalReward) return NULL;
	ucb1Policy->base.applyPolicy = ucb1;
	ucb1Policy->base.freePolicy = freePolicyUcb1;
	ucb1Policy->outFileName = calloc(20, sizeof(char));
	if(!ucb1Policy->outFileName) return NULL;
	return &(ucb1Policy->base);
}

Policy * initPolicyBayesSampling(unsigned int count) {
	BayesSamplingPolicy * bSamplingPolicy = calloc(1, sizeof(BayesSamplingPolicy));
	if(!bSamplingPolicy) return NULL;
	bSamplingPolicy->countSelected = calloc(count, sizeof(unsigned int));
	if(!bSamplingPolicy->countSelected) return NULL;
	bSamplingPolicy->totalReward = calloc(count, sizeof(int));
	if(!bSamplingPolicy->totalReward) return NULL;
	bSamplingPolicy->base.applyPolicy = bayesSampling;
	bSamplingPolicy->base.freePolicy = freePolicyBayesSampling;
	bSamplingPolicy->outFileName = calloc(20, sizeof(char));
	if(!bSamplingPolicy->outFileName) return NULL;
	return &(bSamplingPolicy->base);
}

Policy * initPolicyPreserved(unsigned int count) {
	PreservedPolicy * preservedPolicy = calloc(1, sizeof(PreservedPolicy));
	if(!preservedPolicy) return NULL;
	preservedPolicy->policyValue = calloc(count, sizeof(double));
	if(!preservedPolicy->policyValue) return NULL;
	preservedPolicy->base.applyPolicy = preserved;
	preservedPolicy->base.freePolicy = freePolicyPreserved;
	preservedPolicy->inFileName = calloc(20, sizeof(char));
	if(!preservedPolicy->inFileName) return NULL;
	return &(preservedPolicy->base);
}

void freePolicy(Policy * policy) {
	free(policy);
}

void freePolicySelectMaxNormRand(Policy * policy) {
	SelectMaxNormRandPolicy * sMaxNormRandPolicy = (SelectMaxNormRandPolicy *)policy;
	free(sMaxNormRandPolicy);
}

void freePolicyEpsilonGreedy(Policy * policy) {
	EpsilonGreedyPolicy * eGreedyPolicy = (EpsilonGreedyPolicy *)policy;
	free(eGreedyPolicy);
}

void freePolicyAdaptiveEpsilonGreedy(Policy * policy) {
	AdaptiveEpsilonGreedyPolicy * aEGreedyPolicy = (AdaptiveEpsilonGreedyPolicy *)policy;
	free(aEGreedyPolicy);
}

void freePolicyUcb1(Policy * policy) {
	Ucb1Policy * ucb1Policy = (Ucb1Policy *)policy;
	free(ucb1Policy->totalReward);
	free(ucb1Policy->countSelected);
	free(ucb1Policy->outFileName);
	free(ucb1Policy);
}

void freePolicyBayesSampling(Policy * policy) {
	BayesSamplingPolicy * bSamplingPolicy = (BayesSamplingPolicy *)policy;
	free(bSamplingPolicy->totalReward);
	free(bSamplingPolicy->countSelected);
	free(bSamplingPolicy->outFileName);
	free(bSamplingPolicy);
}

void freePolicyPreserved(Policy * policy) {
	PreservedPolicy * preservedPolicy = (PreservedPolicy *)policy;
	free(preservedPolicy->policyValue);
	free(preservedPolicy->inFileName);
	free(preservedPolicy);
}

int selectMax(Agent * agent, Policy * policy) {
	double max = -1 * __DBL_MAX__;
	bool isMax[] = {false, false, false};
	unsigned int count = 0;
	History * history = ((History *)(agent->histories->tail->data));
	double * Q = history->Q;
	for(unsigned int i = 0; i < agent->countNeuron[agent->countLayer - 1]; i++) if(Q[i] > max) max = Q[i];
	for(unsigned int i = 0; i < agent->countNeuron[agent->countLayer - 1]; i++) {
		if(Q[i] == max) {
			isMax[i] = true;
			count++;
		}
	}
	int rnd = (int)(drand48() * count);
	int d = 0;
	for(unsigned int i = 0; i < agent->countNeuron[agent->countLayer - 1]; i++) {
		if(isMax[i] == true) {
			if(rnd > 0) {
				rnd --;
			} else {
				d = i;
				break;
			}
		}
	}
	return d;
}

int selectMaxNormRand(Agent * agent, Policy * policy) {
	double rand[agent->countNeuron[agent->countLayer - 1]];
	for(unsigned int i = 0; i < agent->countNeuron[agent->countLayer - 1]; i++) rand[i] = normRand(0, ((SelectMaxNormRandPolicy *)(policy))->sigma);
	double max = -1 * __DBL_MAX__;
	bool isMax[] = {false, false, false};
	unsigned int count = 0;
	History * history = ((History *)(agent->histories->tail->data));
	double * Q = history->Q;
	double modQ[agent->countNeuron[agent->countLayer - 1]];
	for(unsigned int i = 0; i < agent->countNeuron[agent->countLayer - 1]; i++) modQ[i] = Q[i] + rand[i];
	for(unsigned int i = 0; i < agent->countNeuron[agent->countLayer - 1]; i++) if(modQ[i] > max) max = modQ[i];
	for(unsigned int i = 0; i < agent->countNeuron[agent->countLayer - 1]; i++) {
		if(modQ[i] == max) {
			isMax[i] = true;
			count++;
		}
	}
	int rnd = (int)(drand48() * count);
	int d = 0;
	for(unsigned int i = 0; i < agent->countNeuron[agent->countLayer - 1]; i++) {
		if(isMax[i] == true) {
			if(rnd > 0) {
				rnd --;
			} else {
				d = i;
				break;
			}
		}
	}
	return d;
}

int epsilonGreedy(Agent * agent, Policy * policy) {
	double max = -1 * __DBL_MAX__;
	bool isMax[] = {false, false, false};
	unsigned int count = 0;
	History * history = ((History *)(agent->histories->tail->data));
	double * Q = history->Q;
	for(unsigned int i = 0; i < agent->countNeuron[agent->countLayer - 1]; i++) if(Q[i] > max) max = Q[i];
	for(unsigned int i = 0; i < agent->countNeuron[agent->countLayer - 1]; i++) {
		if(Q[i] == max) {
			isMax[i] = true;
			count++;
		}
	}
	int d = 0;
	double e = drand48();
	if(e > ((EpsilonGreedyPolicy *)(policy))->e) {
		int rnd = (int)(drand48() * count);
		for(unsigned int i = 0; i < agent->countNeuron[agent->countLayer - 1]; i++) {
			if(isMax[i] == true) {
				if(rnd > 0) {
					rnd --;
				} else {
					d = i;
					break;
				}
			}
		}
	} else {
		int rnd = (int)(drand48() * agent->countNeuron[agent->countLayer - 1] - count);
		for(unsigned int i = 0; i < agent->countNeuron[agent->countLayer - 1]; i++) {
			if(isMax[i] == false) {
				if(rnd > 0) {
					rnd --;
				} else {
					d = i;
					break;
				}
			}
		}
	}
	return d;
}

int adaptiveEpsilonGreedy(Agent * agent, Policy * policy) {
	double max = -1 * __DBL_MAX__;
	bool isMax[] = {false, false, false};
	unsigned int count = 0;
	History * history = ((History *)(agent->histories->tail->data));
	double * Q = history->Q;
	for(unsigned int i = 0; i < agent->countNeuron[agent->countLayer - 1]; i++) if(Q[i] > max) max = Q[i];
	for(unsigned int i = 0; i < agent->countNeuron[agent->countLayer - 1]; i++) {
		if(Q[i] == max) {
			isMax[i] = true;
			count++;
		}
	}
	((AdaptiveEpsilonGreedyPolicy *)(policy))->e -= ((AdaptiveEpsilonGreedyPolicy *)(policy))->atte;
	int d = 0;
	double e = drand48();
	if(e > ((AdaptiveEpsilonGreedyPolicy *)(policy))->e) {
		int rnd = (int)(drand48() * count);
		for(unsigned int i = 0; i < agent->countNeuron[agent->countLayer - 1]; i++) {
			if(isMax[i] == true) {
				if(rnd > 0) {
					rnd --;
				} else {
					d = i;
					break;
				}
			}
		}
	} else {
		int rnd = (int)(drand48() * agent->countNeuron[agent->countLayer - 1] - count);
		for(unsigned int i = 0; i < agent->countNeuron[agent->countLayer - 1]; i++) {
			if(isMax[i] == false) {
				if(rnd > 0) {
					rnd --;
				} else {
					d = i;
					break;
				}
			}
		}
	}
	return d;
}

int ucb1(Agent * agent, Policy * policy) {
	History * currentHistory = ((History *)(agent->histories->tail->data));
	Node * prev = ((Node *)(agent->histories->tail->prev));
	double temp[agent->countNeuron[agent->countLayer - 1]];
	if(prev) {
		History * prevHistory = ((History *)(prev->data));
		Ucb1Policy * uPolicy = ((Ucb1Policy *)(policy));
		uPolicy->countSelected[prevHistory->direction]++;
		uPolicy->totalReward[prevHistory->direction] += (prevHistory->reward + 1);
		unsigned int countAll = 0;
		for(unsigned int i = 0; i < agent->countNeuron[agent->countLayer - 1]; i++) countAll += uPolicy->countSelected[i];
		for(unsigned int i = 0; i < agent->countNeuron[agent->countLayer - 1]; i++) agent->polTemp[i] = uPolicy->totalReward[i] / (countAll + 0.01);
		for(unsigned int i = 0; i < agent->countNeuron[agent->countLayer - 1]; i++) agent->polTemp[i] += sqrt(uPolicy->c * log(countAll) / (uPolicy->countSelected[i] + 0.01));
	}
	double max = -1 * __DBL_MAX__;
	bool isMax[] = {false, false, false};
	unsigned int count = 0;
	double * Q = currentHistory->Q;
	for(unsigned int i = 0; i < agent->countNeuron[agent->countLayer - 1]; i++) temp[i] = Q[i] * agent->polTemp[i];
	for(unsigned int i = 0; i < agent->countNeuron[agent->countLayer - 1]; i++) if(temp[i] > max) max = temp[i];
	for(unsigned int i = 0; i < agent->countNeuron[agent->countLayer - 1]; i++) {
		if(temp[i] == max) {
			isMax[i] = true;
			count++;
		}
	}
	int rnd = (int)(drand48() * count);
	int d = 0;
	for(unsigned int i = 0; i < agent->countNeuron[agent->countLayer - 1]; i++) {
		if(isMax[i] == true) {
			if(rnd > 0) {
				rnd --;
			} else {
				d = i;
				break;
			}
			d = i;
		}
	}
	return d;
}

int bayesSampling(Agent * agent, Policy * policy) {
	History * currentHistory = ((History *)(agent->histories->tail->data));
	Node * prev = ((Node *)(agent->histories->tail->prev));
	double temp[agent->countNeuron[agent->countLayer - 1]];
	if(prev) {
		History * prevHistory = ((History *)(prev->data));
		BayesSamplingPolicy * bSamplingPolicy = ((BayesSamplingPolicy *)(policy));
		bSamplingPolicy->countSelected[prevHistory->direction]++;
		bSamplingPolicy->totalReward[prevHistory->direction] += prevHistory->reward;
		for(unsigned int i = 0; i < agent->countNeuron[agent->countLayer - 1]; i++) agent->polTemp[i] = (double)bSamplingPolicy->totalReward[i] / (1 + bSamplingPolicy->countSelected[i]);
		for(unsigned int i = 0; i < agent->countNeuron[agent->countLayer - 1]; i++) agent->polTemp[i] += normRand(0, 1) / sqrt(1 + bSamplingPolicy->countSelected[i]);
	}
	double max = -1 * __DBL_MAX__;
	bool isMax[] = {false, false, false};
	unsigned int count = 0;
	double * Q = currentHistory->Q;
	for(unsigned int i = 0; i < agent->countNeuron[agent->countLayer - 1]; i++) temp[i] = agent->polTemp[i] * Q[i];
	for(unsigned int i = 0; i < agent->countNeuron[agent->countLayer - 1]; i++) if(temp[i] > max) max = temp[i];
	for(unsigned int i = 0; i < agent->countNeuron[agent->countLayer - 1]; i++) {
		if(temp[i] == max) {
			isMax[i] = true;
			count++;
		}
	}
	int rnd = (int)(drand48() * count);
	int d = 0;
	for(unsigned int i = 0; i < agent->countNeuron[agent->countLayer - 1]; i++) {
		if(isMax[i] == true) {
			if(rnd > 0) {
				rnd --;
			} else {
				d = i;
				break;
			}
			d = i;
		}
	}
	return d;
}

int preserved(Agent * agent, Policy * policy) {
	for(unsigned int i = 0; i < agent->countNeuron[agent->countLayer - 1]; i++) agent->polTemp[i] = ((PreservedPolicy *)policy)->policyValue[i];
	double temp[agent->countNeuron[agent->countLayer - 1]];
	double max = -1 * __DBL_MAX__;
	bool isMax[] = {false, false, false};
	unsigned int count = 0;
	History * history = ((History *)(agent->histories->tail->data));
	double * Q = history->Q;
	for(unsigned int i = 0; i < agent->countNeuron[agent->countLayer - 1]; i++) temp[i] = agent->polTemp[i] * Q[i];
	for(unsigned int i = 0; i < agent->countNeuron[agent->countLayer - 1]; i++) if(temp[i] > max) max = temp[i];
	for(unsigned int i = 0; i < agent->countNeuron[agent->countLayer - 1]; i++) {
		if(temp[i] == max) {
			isMax[i] = true;
			count++;
		}
	}
	int rnd = (int)(drand48() * count);
	int d = 0;
	for(unsigned int i = 0; i < agent->countNeuron[agent->countLayer - 1]; i++) {
		if(isMax[i] == true) {
			if(rnd > 0) {
				rnd --;
			} else {
				d = i;
				break;
			}
			d = i;
		}
	}
	return d;
}

Policy * parsePolicyOptionSelectMaxNormRand(char * policyParams, Policy * policy){
	char * query, * pPolicyParams;
	double sigma;
	query = "sigma=";
	pPolicyParams = strstr(policyParams, query);
	if(!pPolicyParams) return NULL;
	else pPolicyParams = strchr(pPolicyParams, '=');
	sigma = atof(pPolicyParams + 1);
	return initPolicySelectMaxNormRand(sigma);
}

Policy * parsePolicyOptionEpsilonGreedy(char * policyParams, Policy * policy){
	char * query, * pPolicyParams;
	double epsilon;
	query = "epsilon=";
	pPolicyParams = strstr(policyParams, query);
	if(!pPolicyParams) return NULL;
	else pPolicyParams = strchr(pPolicyParams, '=');
	epsilon = atof(pPolicyParams + 1);
	return initPolicyEpsilonGreedy(epsilon);
}

Policy * parsePolicyOptionAdaptiveEpsilonGreedy(char * policyParams, Policy * policy){
	char * query, * pPolicyParams;
	double atte, e;
	query = "atte=";
	pPolicyParams = strstr(policyParams, query);
	if(!pPolicyParams) return NULL;
	else pPolicyParams = strchr(pPolicyParams, '=');
	atte = atof(pPolicyParams + 1);
	query = "epsilon=";
	pPolicyParams = strstr(pPolicyParams, query);
	if(!pPolicyParams) return NULL;
	else pPolicyParams = strchr(pPolicyParams, '=');
	e = atof(pPolicyParams + 1);
	return initPolicyAdaptiveEpsilonGreedy(atte, e);
}

Policy * parsePolicyOptionUcb1(char * policyParams, Policy * policy){
	char * query, * pPolicyParams;
	double c;
	query = "c=";
	pPolicyParams = strstr(policyParams, query);
	if(!pPolicyParams) return NULL;
	else pPolicyParams = strchr(pPolicyParams, '=');
	c = atof(pPolicyParams + 1);
	Policy * ret = initPolicyUcb1(3, c);
	query = "outFileName=";
	pPolicyParams = strstr(policyParams, query);
	if(!pPolicyParams) return NULL;
	else pPolicyParams = strchr(pPolicyParams, '=');
	strncpy(((Ucb1Policy *)(ret))->outFileName, pPolicyParams + 1, 20);
	return ret;
}

Policy * parsePolicyOptionBayesSampling(char * policyParams, Policy * policy){
	char * query, * pPolicyParams;
	Policy * ret = initPolicyBayesSampling(3);
	query = "outFileName=";
	pPolicyParams = strstr(policyParams, query);
	if(!pPolicyParams) return NULL;
	else pPolicyParams = strchr(pPolicyParams, '=');
	strncpy(((BayesSamplingPolicy *)(ret))->outFileName, pPolicyParams + 1, 20);
	return ret;
}

Policy * parsePolicyOptionPreserved(char * policyParams, Policy * policy){
	char * query, * pPolicyParams;
	Policy * ret = initPolicyPreserved(3);
	query = "inFileName=";
	pPolicyParams = strstr(policyParams, query);
	if(!pPolicyParams) return NULL;
	else pPolicyParams = strchr(pPolicyParams, '=');
	strncpy(((PreservedPolicy *)(ret))->inFileName, pPolicyParams + 1, 20);
	return ret;
}

