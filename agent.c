#include "include/agent.h"
#include "include/td.h"
#include "include/policy.h"
#include <stdlib.h>
#include <stdbool.h>
#include <threads.h>

void initNetwork(Agent * agent) {
	agent->countLayer = 3;
	agent->countNeuron[0] = agent->countSensor;
	agent->countNeuron[agent->countLayer - 1] = 3;
	agent->network = createNetwork(agent->countLayer, agent->countNeuron);
	Node * current = agent->network->head;
	while(current) {
		((Net *)(current->data))->activate = tanh;
		((Net *)(current->data))->deactivate = tanh_b;
		((Net *)(current->data))->forward = forward;
		((Net *)(current->data))->backward = backward;
		((Net *)(current->data))->alpha = agent->netAlpha;
		((Net *)(current->data))->randMax = agent->netRandMax;
		initWeight((Net*)(current->data));
		current = current->next;
	}
	agent->targetNetwork = createNetwork(agent->countLayer, agent->countNeuron);
	Node * targetCurrent = agent->targetNetwork->head;
	while(targetCurrent) {
		((Net *)(targetCurrent->data))->activate = tanh;
		((Net *)(targetCurrent->data))->deactivate = tanh_b;
		((Net *)(targetCurrent->data))->forward = forward;
		((Net *)(targetCurrent->data))->backward = backward;
		((Net *)(targetCurrent->data))->alpha = agent->netAlpha;
		((Net *)(targetCurrent->data))->randMax = agent->netRandMax;
		initWeight((Net*)(targetCurrent->data));
		targetCurrent = targetCurrent->next;
	}
	current = agent->network->head;
	targetCurrent = agent->targetNetwork->head;
	while(current) {
		copyWeight((Net *)(current->data), (Net *)(targetCurrent->data));
		current = current->next;
		targetCurrent = targetCurrent->next;
	}
	agent->accumulatedWeights = createNetwork(agent->countLayer, agent->countNeuron);
	Node * weightsCurrent = agent->accumulatedWeights->head;
	while(weightsCurrent) {
		((Net *)(weightsCurrent->data))->activate = tanh;
		((Net *)(weightsCurrent->data))->deactivate = tanh_b;
		((Net *)(weightsCurrent->data))->forward = forward;
		((Net *)(weightsCurrent->data))->backward = backward;
		((Net *)(weightsCurrent->data))->alpha = agent->netAlpha;
		((Net *)(weightsCurrent->data))->randMax = 0;
		initWeight((Net*)(weightsCurrent->data));
		weightsCurrent = weightsCurrent->next;
	}
	agent->iterTargetNetwork = 0;
	agent->histories = calloc(sizeof(List), 1);
	if(!agent->histories) exit(EXIT_FAILURE);
}

void calculateQ(Agent * agent) {
	History * history = ((History *)(agent->histories->tail->data));
	double * Q = predict(agent->network, history->sensor, agent->countNeuron[0]);
	for(unsigned int i = 0; i < agent->countNeuron[agent->countLayer - 1]; i++) history->Q[i] = Q[i];
}

int decideAction(Agent * agent) {
	int d = agent->policy->applyPolicy(agent, (Policy *)(agent->policy));
	History * history = ((History *)(agent->histories->tail->data));
	history->direction = d;
	return d;
}

double updateNet(Agent * agent, Agent * sharedAgent) {
	Agent * targetAgent = agent;
	if(sharedAgent) targetAgent = sharedAgent;
	if(agent->td->getTd == getZeroStepSarsaTd) {
		ZeroStepSarsaTd * zeroStepSarsaTd = ((ZeroStepSarsaTd *)(agent->td));
		agent->QTargetNetwork = predict(agent->network, ((History *)(agent->histories->tail->data))->sensor, agent->countNeuron[0]);
		double * error = agent->td->getTd(agent);
		double * Q = predict(agent->network, ((History *)(zeroStepSarsaTd->targetUpdate))->sensor, agent->countNeuron[0]);
		loss(agent->network, targetAgent->accumulatedWeights, error, agent->countNeuron[agent->countLayer - 1]);
		free(error);
		Node * current = agent->network->head;
		Node * targetCurrent = agent->targetNetwork->head;
		if(targetAgent->iterTargetNetwork % agent->periodUpdateTargetNetwork == 0) {
			while(current) {
				copyWeight((Net *)(current->data), (Net *)(targetCurrent->data));
				current = current->next;
				targetCurrent = targetCurrent->next;
			}
			targetAgent->iterTargetNetwork = 0;
		}
	} else if(agent->td->getTd == getZeroStepQTd) {
		ZeroStepQTd * zeroStepQTd = ((ZeroStepQTd *)(agent->td));
		agent->QTargetNetwork = predict(agent->network, ((History *)(agent->histories->tail->data))->sensor, agent->countNeuron[0]);
		double * error = agent->td->getTd(agent);
		double * Q = predict(agent->network, ((History *)(zeroStepQTd->targetUpdate))->sensor, agent->countNeuron[0]);
		loss(agent->network, targetAgent->accumulatedWeights, error, agent->countNeuron[agent->countLayer - 1]);
		free(error);
		Node * current = agent->network->head;
		Node * targetCurrent = agent->targetNetwork->head;
		if(targetAgent->iterTargetNetwork % agent->periodUpdateTargetNetwork == 0) {
			while(current) {
				copyWeight((Net *)(current->data), (Net *)(targetCurrent->data));
				current = current->next;
				targetCurrent = targetCurrent->next;
			}
			targetAgent->iterTargetNetwork = 0;
		}
	} else if(agent->td->getTd == getNStepSarsaTd) {
		double * Q;
		agent->QTargetNetwork = predict(agent->network, ((History *)(agent->histories->tail->data))->sensor, agent->countNeuron[0]);
		if(((NStepSarsaTd *)(agent->td))->totalCount == 0) Q = predict(agent->network, ((History *)(agent->histories->head->data))->sensor, agent->countNeuron[0]);
		else Q = predict(agent->network, ((History *)(((NStepSarsaTd *)(agent->td))->targetUpdate))->sensor, agent->countNeuron[0]);
		double * error = agent->td->getTd(agent);
		loss(agent->network, targetAgent->accumulatedWeights, error, agent->countNeuron[agent->countLayer - 1]);
		free(error);
		Node * current = agent->network->head;
		Node * targetCurrent = agent->targetNetwork->head;
		if(targetAgent->iterTargetNetwork % agent->periodUpdateTargetNetwork == 0) {
			while(current) {
				copyWeight((Net *)(current->data), (Net *)(targetCurrent->data));
				current = current->next;
				targetCurrent = targetCurrent->next;
			}
			targetAgent->iterTargetNetwork = 0;
		}
	} else if(agent->td->getTd == getNStepQTd) {
		double * Q;
		agent->QTargetNetwork = predict(agent->network, ((History *)(agent->histories->tail->data))->sensor, agent->countNeuron[0]);
		if(((NStepQTd *)(agent->td))->totalCount == 0) Q = predict(agent->network, ((History *)(agent->histories->head->data))->sensor, agent->countNeuron[0]);
		else Q = predict(agent->network, ((History *)(((NStepQTd *)(agent->td))->targetUpdate))->sensor, agent->countNeuron[0]);
		double * error = agent->td->getTd(agent);
		loss(agent->network, targetAgent->accumulatedWeights, error, agent->countNeuron[agent->countLayer - 1]);
		free(error);
		Node * current = agent->network->head;
		Node * targetCurrent = agent->targetNetwork->head;
		if(targetAgent->iterTargetNetwork % agent->periodUpdateTargetNetwork == 0) {
			while(current) {
				copyWeight((Net *)(current->data), (Net *)(targetCurrent->data));
				current = current->next;
				targetCurrent = targetCurrent->next;
			}
			targetAgent->iterTargetNetwork = 0;
		}
	}
	targetAgent->iterTargetNetwork++;
}

History * allocHistory(unsigned int countInput, unsigned int countOutput) {
	History * ret = calloc(1, sizeof(History));
	if(!ret) return NULL;
	ret->Q = calloc(countOutput, sizeof(double));
	if(!ret->Q) return NULL;
	ret->sensor = calloc(countInput, sizeof(double));
	if(!ret->sensor) return NULL;
	return ret;
}

void freeHistory(History * history) {
	if(history) {
		if(history->Q) free(history->Q);
		if(history->sensor) free(history->sensor);
		free(history);
	}
}

void destroyHistoryList(List * list) {
	Node * current = list->head;
	while(current) {
		freeHistory(current->data);
		current = current->next;
	}
	while(list->head) delTail(list);
	free(list);
}

int importAgentValues(Agent * agent) {
	if(agent->fileInput && (strcmp(agent->fileInput, "") != 0)) {
                List * network = importNetwork(agent->fileInput);
		if(!network) return -1;
		Node * current = network->head;
		Node * agentCurrent = agent->network->head;
		while(current) {
			copyWeight(((Net *)(current->data)), ((Net *)(agentCurrent->data)));
			current = current->next;
			agentCurrent = agentCurrent->next;
		}
		destroyNetwork(network);
		if(((PreservedPolicy *)(agent->policy))->base.applyPolicy == preserved) {
                        FILE * fp = fopen(((PreservedPolicy *)(agent->policy))->inFileName, "r");
                        if(!fp) return -1;
			char buf[20];
			for(unsigned int i = 0; i < agent->countNeuron[agent->countLayer - 1]; i++) {
				fgets(buf, 20, fp);
				((PreservedPolicy *)(agent->policy))->policyValue[i] = atof(buf);
			}
			fclose(fp);
		}
	}
	return 0;
}

int prepareExportAgentValue(Agent * agent) {
	char fileName[50];
	sprintf(fileName, "QUpdateHistory_%lu.dat", thrd_current());
	agent->fpQUpdate = fopen(fileName, "w");
	if(!agent->fpQUpdate) return -1;
}

int exportAgentValue(Agent * agent) {
	if(agent->fileOutput && strcmp(agent->fileOutput, "") != 0)	{
		exportNetwork(agent->network, agent->fileOutput);
		if(((Ucb1Policy *)(agent->policy))->base.applyPolicy == ucb1) {
			FILE * fp = fopen(((Ucb1Policy *)(agent->policy))->outFileName, "w");
			if(!fp) return -1;
			for(unsigned int i = 0; i < agent->countNeuron[agent->countLayer - 1]; i++) fprintf(fp, "%lf\n", agent->polTemp[i]);
			fclose(fp);
		}
		if(((BayesSamplingPolicy *)(agent->policy))->base.applyPolicy == bayesSampling) {
			FILE * fp = fopen(((BayesSamplingPolicy *)(agent->policy))->outFileName, "w");
			if(!fp) return -1;
			for(unsigned int i = 0; i < agent->countNeuron[agent->countLayer - 1]; i++) fprintf(fp, "%lf\n", agent->polTemp[i]);
			fclose(fp);
		}
	}
}

void initAgent(Agent * agent) {
	initNetwork(agent);
	for(unsigned int i = 0; i < agent->countNeuron[agent->countLayer - 1]; i++) agent->polTemp[i] = 0;
	importAgentValues(agent);
	prepareExportAgentValue(agent);
}

void freeAgent(Agent * agent) {
	if(agent->fpQUpdate) fclose(agent->fpQUpdate);
	agent->policy->freePolicy(agent->policy);
	agent->td->free(agent->td);
	free(agent->fileOutput);
	free(agent->fileInput);
	free(agent->countNeuron);
	destroyNetwork(agent->network);
	destroyNetwork(agent->targetNetwork);
	destroyNetwork(agent->accumulatedWeights);
	destroyHistoryList(agent->histories);
}

void addHistory(Agent * agent) {
	addTail(agent->histories);
	agent->histories->tail->data = allocHistory(agent->countNeuron[0], agent->countNeuron[agent->countLayer - 1]);
}

void forwardAgentStep(Agent * agent) {
	calculateQ(agent);
	decideAction(agent);
}
