.PHONY all: agent.o td.o policy.o test
agent.o: agent.c include/common.h include/agent.h include/td.h include/policy.h
	cc -c agent.c -g
td.o: td.c include/common.h include/agent.h include/td.h
	cc -c td.c -g
policy.o: policy.c include/common.h include/agent.h include/policy.h
	cc -c policy.c -g
test: test/*
	make all -C test/
