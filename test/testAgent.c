#include "../include/nnet.h"
#include "../include/nlist.h"
#include "../include/nmath.h"
#include "../include/nassert.h"
#include "../include/agent.h"
#include "../include/policy.h"
#include <assert.h>
#include <time.h>
#include <stdlib.h>

Agent * initAgentOnTest(void) {
	Agent * agent = calloc(1, sizeof(Agent));
	assert(agent);
	agent->countLayer = 3;
	agent->countNeuron = calloc(agent->countLayer, sizeof(unsigned int));
	assert(agent->countNeuron);
	agent->countNeuron[0] = 8;
	agent->countNeuron[agent->countLayer - 1] = 3;
	agent->histories = calloc(1, sizeof(List));
	assert(agent->histories);
	addTail(agent->histories);
	assert(agent->histories->tail);
	agent->histories->tail->data = allocHistory(agent->countNeuron[0], agent->countNeuron[agent->countLayer - 1]);
	assert(agent->histories->tail->data);
	return agent;
}

void freeAgentOnTest(Agent * agent) {
	agent->policy->freePolicy(agent->policy);
	destroyHistoryList(agent->histories);
	free(agent->countNeuron);
	free(agent);
}

void testDecideAction(void) {
	Agent * agent = initAgentOnTest();
	agent->policy = initPolicySelectMax();
	assert(agent->policy);
	((History *)(agent->histories->tail->data))->Q[0] = 0.1;
	((History *)(agent->histories->tail->data))->Q[1] = 0.3;
	((History *)(agent->histories->tail->data))->Q[2] = 0.1;
	int d = decideAction(agent);
	assert(d == 1);
	freeAgentOnTest(agent);
}

void testDecideAction2(void) {
	Agent * agent = initAgentOnTest();
	agent->policy = initPolicySelectMax();
	assert(agent->policy);
	((History *)(agent->histories->tail->data))->Q[0] = 0.1;
	((History *)(agent->histories->tail->data))->Q[1] = 0.3;
	((History *)(agent->histories->tail->data))->Q[2] = 0.3;
	int d = decideAction(agent);
	assert(d == 1 || d == 2);
	freeAgentOnTest(agent);
}

void testDecideAction3(void) {
	Agent * agent = initAgentOnTest();
	agent->policy = initPolicySelectMax();
	assert(agent->policy);
	((History *)(agent->histories->tail->data))->Q[0] = 0.3;
	((History *)(agent->histories->tail->data))->Q[1] = 0.3;
	((History *)(agent->histories->tail->data))->Q[2] = 0.3;
	int d = decideAction(agent);
	assert(d == 0 || d == 1 || d == 2);
	freeAgentOnTest(agent);
}

void testDecideAction4(void) {
	Agent * agent = initAgentOnTest();
	agent->policy = initPolicySelectMax();
	assert(agent->policy);
	((History *)(agent->histories->tail->data))->Q[0] = 0.3;
	((History *)(agent->histories->tail->data))->Q[1] = 0.1;
	((History *)(agent->histories->tail->data))->Q[2] = 0.3;
	int d = decideAction(agent);
	assert(d == 0 || d == 2);
	freeAgentOnTest(agent);
}

int main(void) {
	srand48(time(NULL));
	testDecideAction();
	testDecideAction2();
	testDecideAction3();
	testDecideAction4();
}
