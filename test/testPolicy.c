#include "../include/nnet.h"
#include "../include/nlist.h"
#include "../include/nmath.h"
#include "../include/nassert.h"
#include "../include/agent.h"
#include "../include/policy.h"
#include <assert.h>
#include <time.h>
#include <stdlib.h>

Agent * initAgentOnTest(void) {
	Agent * agent = calloc(1, sizeof(Agent));
	assert(agent);
	agent->countLayer = 3;
	agent->countNeuron = calloc(agent->countLayer, sizeof(unsigned int));
	assert(agent->countNeuron);
	agent->countNeuron[0] = 8;
	agent->countNeuron[agent->countLayer - 1] = 3;
	agent->histories = calloc(1, sizeof(List));
	assert(agent->histories);
	addTail(agent->histories);
	assert(agent->histories->tail);
	agent->histories->tail->data = allocHistory(agent->countNeuron[0], agent->countNeuron[agent->countLayer - 1]);
	assert(agent->histories->tail->data);
	return agent;
}

void freeAgentOnTest(Agent * agent) {
	agent->policy->freePolicy(agent->policy);
	destroyHistoryList(agent->histories);
	free(agent->countNeuron);
	free(agent);
}

void testSelectMaxPolicy(void) {
	Agent * agent = initAgentOnTest();
	agent->policy = initPolicySelectMax();
	assert(agent->policy->applyPolicy == selectMax);
	freeAgentOnTest(agent);
}

void testSelectMaxNormRandPolicy(void) {
	Agent * agent = initAgentOnTest();
	agent->policy = initPolicySelectMaxNormRand(0.2);
	assert(agent->policy->applyPolicy == selectMaxNormRand);
	assert(((SelectMaxNormRandPolicy *)(agent->policy))->sigma == 0.2);
	freeAgentOnTest(agent);
}

void testEpsilonGreedyPolicy(void) {
	Agent * agent = initAgentOnTest();
	agent->policy = initPolicyEpsilonGreedy(0.05);
	assert(agent->policy->applyPolicy == epsilonGreedy);
	assert(((EpsilonGreedyPolicy *)(agent->policy))->e == 0.05);
	freeAgentOnTest(agent);
}

void testAdaptiveEpsilonGreedyPolicy(void) {
	Agent * agent = initAgentOnTest();
	agent->policy = initPolicyAdaptiveEpsilonGreedy(0.05/3000, 0.05);
	assert(agent->policy->applyPolicy == adaptiveEpsilonGreedy);
	assert(((AdaptiveEpsilonGreedyPolicy *)(agent->policy))->atte == 0.05/3000);
	assert(((AdaptiveEpsilonGreedyPolicy *)(agent->policy))->e == 0.05);
	freeAgentOnTest(agent);
}

int main(void) {
	srand48(time(NULL));
	testSelectMaxPolicy();
	testSelectMaxNormRandPolicy();
	testEpsilonGreedyPolicy();
	testAdaptiveEpsilonGreedyPolicy();
}
