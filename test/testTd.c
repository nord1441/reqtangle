#include "../include/nnet.h"
#include "../include/nlist.h"
#include "../include/nmath.h"
#include "../include/nassert.h"
#include "../include/agent.h"
#include "../include/policy.h"
#include "../include/td.h"
#include <assert.h>
#include <time.h>
#include <stdlib.h>

Agent * initAgentOnTest(void) {
	Agent * agent = calloc(1, sizeof(Agent));
	assert(agent);
	agent->countLayer = 3;
	agent->countNeuron = calloc(agent->countLayer, sizeof(unsigned int));
	assert(agent->countNeuron);
	agent->countNeuron[0] = 8;
	agent->countNeuron[agent->countLayer - 1] = 3;
	agent->histories = calloc(1, sizeof(List));
	assert(agent->histories);
	addTail(agent->histories);
	assert(agent->histories->tail);
	agent->histories->tail->data = allocHistory(agent->countNeuron[0], agent->countNeuron[agent->countLayer - 1]);
	assert(agent->histories->tail->data);
	agent->QTargetNetwork = calloc(agent->countNeuron[agent->countLayer - 1], sizeof(double));
	return agent;
}

void freeAgentOnTest(Agent * agent) {
	agent->policy->freePolicy(agent->policy);
	destroyHistoryList(agent->histories);
	free(agent->QTargetNetwork);
	free(agent->countNeuron);
	free(agent);
}

void testGetZeroStepSarsaTd() {
	double gamma = 0.7;
	Agent * agent = initAgentOnTest();
	agent->policy = initPolicySelectMax();
	unsigned int iter = 10;
	agent->td = initTdZeroStepSarsa(gamma);
	double Q[3][10] = {
		{0.1, 0.4, 0.7, 1.0, 1.3, 1.6, 1.9, 2.2, 2.5, 2.8},
		{0.2, 0.5, 0.8, 1.1, 1.4, 1.7, 2.0, 2.3, 2.6, 2.9},
		{0.3, 0.6, 0.9, 1.2, 1.5, 1.8, 2.1, 2.4, 2.7, 3.0}
	};
	int reward[10] = {0, -1, 0, 0, 0, 1, 0, 0, 1, -1};
	int direction[10] = {0, 1, 0, 2, 2, 1, 0, 0, 1, 2};
	double * error[10];
	double assumedError[iter][agent->countNeuron[agent->countLayer - 1]];
	ZeroStepSarsaTd * zeroStepSarsaTd = (ZeroStepSarsaTd *)(agent->td);
	for(int j = iter - 1; j >= 0; j--) {
		for(unsigned int i = 0; i < agent->countNeuron[agent->countLayer - 1]; i++) {
			if(j == 0) assumedError[j][i] = 0;
			else {
				if(i == direction[j - 1]) {
					assumedError[j][i] = 0;
					assumedError[j][i] = reward[j] - Q[direction[j - 1]][j - 1];
					if(reward[j] == 0) assumedError[j][i] += gamma * Q[direction[j]][j];
				} else {
					assumedError[j][i] = 0;
				}
			}
		}
	}
	for(unsigned int j = 0; j < iter; j++) {
		for(unsigned int i = 0; i < agent->countNeuron[agent->countLayer - 1]; i++) {
			((History *)(agent->histories->tail->data))->Q[i] = Q[i][j];
			agent->QTargetNetwork[i] = Q[i][j];
		}
		((History *)(agent->histories->tail->data))->reward = reward[j];
		((History *)(agent->histories->tail->data))->direction = direction[j];
		ZeroStepSarsaTd * zeroStepSarsaTd = (ZeroStepSarsaTd *)(agent->td);
		if(j > 0) {
			error[j] = agent->td->getTd(agent);
			assert(zeroStepSarsaTd->targetUpdate == agent->histories->tail->prev->data);
		} else {
			error[j] = agent->td->getTd(agent);
			assert(zeroStepSarsaTd->targetUpdate == agent->histories->head->data);
		}
		addTail(agent->histories);
		agent->histories->tail->data = allocHistory(agent->countNeuron[0], agent->countNeuron[agent->countLayer - 1]);
	}
	for(unsigned int j = 0; j < iter; j++) {
		for(unsigned int i = 0; i < agent->countNeuron[agent->countLayer - 1]; i++) {
			assertEqual(error[j][i], assumedError[j][i], 0.0001);
		}
	}
	for(unsigned int j = 0; j < iter; j++) {
		free(error[j]);
	}
	agent->td->free(agent->td);
	freeAgentOnTest(agent);
}

void testInitTdNStepSarsa(void) {
	Agent * agent = initAgentOnTest();
	agent->policy = initPolicySelectMax();
	assert(agent->policy);
	double gamma = 0.7;
	unsigned int intervalUpdate = 2;
	agent->td = initTdNStepSarsa(gamma, intervalUpdate);
	assert(((NStepSarsaTd *)(agent->td))->gamma == gamma);
	assert(((NStepSarsaTd *)(agent->td))->intervalUpdate == intervalUpdate);
	assert(agent->td->getTd == getNStepSarsaTd);
	assert(agent->td->free == freeTdNStepSarsa);
	assert(((NStepSarsaTd *)(agent->td))->histories);
	agent->td->free(agent->td);
	freeAgentOnTest(agent);
}

void testGetNStepSarsaTd(unsigned int intervalUpdate) {
	double gamma = 0.7;
	Agent * agent = initAgentOnTest();
	agent->policy = initPolicySelectMax();
	unsigned int iter = 10;
	agent->td = initTdNStepSarsa(gamma, intervalUpdate);
	double Q[3][10] = {
		{0.1, 0.4, 0.7, 1.0, 1.3, 1.6, 1.9, 2.2, 2.5, 2.8},
		{0.2, 0.5, 0.8, 1.1, 1.4, 1.7, 2.0, 2.3, 2.6, 2.9},
		{0.3, 0.6, 0.9, 1.2, 1.5, 1.8, 2.1, 2.4, 2.7, 3.0}
	};
	int reward[10] = {0, -1, 0, 0, 0, 1, 0, 0, 1, -1};
	int direction[10] = {0, 1, 0, 2, 2, 1, 0, 0, 1, 2};
	double * error[10];
	double assumedError[iter][agent->countNeuron[agent->countLayer - 1]];
	NStepSarsaTd * nStepSarsaTd = (NStepSarsaTd *)(agent->td);
	int head = 0;
	for(unsigned int j = 0; j < iter; j++) {
		for(unsigned int i = 0; i < agent->countNeuron[agent->countLayer - 1]; i++) {
			if(j == 0) assumedError[j][i] = 0;
			if(j != head + nStepSarsaTd->intervalUpdate != 0 && reward[j] == 0) {
				assumedError[j][i] = 0;
			} else {
				if(i == direction[head]) {
					assumedError[j][i] = 0;
					assumedError[j][i] -= Q[direction[head]][head];
					unsigned int k = 0;
					double tempGamma = 1;
					do {
						k++;
						assumedError[j][i] += tempGamma * reward[head + k];
						tempGamma *= gamma;
					} while(head + k < j);
					if(k > 1) assumedError[j][i] += gamma * Q[direction[head + k]][head + k];
				} else {
					assumedError[j][i] = 0;
				}
			}
		}
		if(j != 0 && (j == head + nStepSarsaTd->intervalUpdate || reward[j] != 0)) head = j;
	}
	head = 0;
	for(unsigned int j = 0; j < iter; j++) {
		for(unsigned int i = 0; i < agent->countNeuron[agent->countLayer - 1]; i++) {
			((History *)(agent->histories->tail->data))->Q[i] = Q[i][j];
			agent->QTargetNetwork[i] = Q[i][j];
		}
		((History *)(agent->histories->tail->data))->reward = reward[j];
		((History *)(agent->histories->tail->data))->direction = direction[j];
		error[j] = agent->td->getTd(agent);
		NStepSarsaTd * nStepSarsaTd = (NStepSarsaTd *)(agent->td);
		History * assumedTargetUpdate;
		if(j == 0 || (j == head + nStepSarsaTd->intervalUpdate || reward[j] != 0)) assumedTargetUpdate = agent->histories->tail->data;
		assert(nStepSarsaTd->targetUpdate == assumedTargetUpdate);
		addTail(agent->histories);
		agent->histories->tail->data = allocHistory(agent->countNeuron[0], agent->countNeuron[agent->countLayer - 1]);
		if(j != 0 && (j == head + nStepSarsaTd->intervalUpdate || reward[j] != 0)) head = j;
	}
	for(unsigned int j = 0; j < iter; j++) {
		for(unsigned int i = 0; i < agent->countNeuron[agent->countLayer - 1]; i++) {
			assertEqual(error[j][i], assumedError[j][i], 0.0001);
		}
	}
	for(unsigned int j = 0; j < iter; j++) {
		free(error[j]);
	}
	agent->td->free(agent->td);
	freeAgentOnTest(agent);
}

int main(void) {
	srand48(time(NULL));
	testInitTdNStepSarsa();
	testGetZeroStepSarsaTd();
	testGetNStepSarsaTd(1);
	testGetNStepSarsaTd(2);
	testGetNStepSarsaTd(3);
	testGetNStepSarsaTd(4);
	testGetNStepSarsaTd(5);
}
